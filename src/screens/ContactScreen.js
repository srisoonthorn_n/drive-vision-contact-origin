import React from 'react'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import ProductDetail from '../components/ProductDetail'
import CustomerInfo from '../components/CustomerInfo';
import CompanyInfo from '../components/CompanyInfo';
import BusinessInfo from '../components/BusinessInfo';
import Button from '../components/common/Button';

export default function ContactScreen() {
    const classes = useStyles();

    return (
        <Grid container alignItems="center" direction="column" style={{ backgroundColor: "#FAFAFA", paddingBottom: 110 }}>

            <ProductDetail />

            <Grid item xs={8} lg={5} container justify="center" direction="row" style={{ marginBottom: 18 }}>
                <Grid >
                    <Typography style={{ color: "#F38181", fontSize: 14, marginRight: 8 }}>**</Typography>
                </Grid>

                <Grid>
                    <Typography style={{ color: "#7E7E7E", fontSize: 14, textAlign: "center" }}>โปรดกรอกรายละเอียด เพื่อที่ทางบริษัทจะได้ติดต่อกลับได้อย่างรวดเร็วและถูกต้อง</Typography>
                </Grid>

            </Grid>

            <CustomerInfo />

            <CompanyInfo />

            <BusinessInfo />

            <Grid item xs={8} md={5} lg={3} container>
                <Button text={"บันทึกข้อมูล"} />
            </Grid>
        </Grid>
    )

}


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: 24
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    listDrawer: {
        width: 250,
    },
    fullListDrawer: {
        width: 'auto',
    },
}));
