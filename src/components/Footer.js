import React from 'react'
import { Grid, Hidden } from '@material-ui/core';

const Footer = ({ width = "90vw", position = 'initial', ...props }) => {
    return <footer style={{ backgroundColor: "#FFC5C5", bottom: 0, margin: 0, position, width, ...props.style }} >
        <Hidden smDown>
            <span className={"primary-text"} style={{ justifyContent: 'center', display: 'flex', alignItems: 'center', fontSize: 14, paddingTop: 16, paddingLeft: 30, paddingRight: 30, lineHeight: 2, paddingBottom: 16 }}>
                {/* <span style={{ fontFamily: 'San', fontSize: 18, margin: 5 }}>©</span> */}
            CopyRight © Dev Hero Co., Ltd. 2020., All Right Reserverd, Chiang Mai, Thailand.
            </span>
        </Hidden>
        <Hidden mdUp>
            <span className={"primary-text"} style={{ justifyContent: 'center', display: 'flex', alignItems: 'center', fontSize: 10, paddingTop: 16, paddingLeft: 10, paddingRight: 10, lineHeight: 2, paddingBottom: 16 }}>

                CopyRight © Dev Hero Co., Ltd. 2020., All Right Reserverd, Chiang Mai, Thailand.
            </span>
        </Hidden>



    </footer>
}

export default Footer;
