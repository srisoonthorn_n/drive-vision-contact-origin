import React, { useState } from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from './TextField';
import { Grid, Typography, withStyles } from '@material-ui/core';


// const DefaultRadio = withStyles({
//     root: {
//         color: "#F38181",
//         '&$checked': {
//             color: "#F38181",
//         },
//     },
//     checked: {},
// })((props) => <RadioGroup color="default" {...props} />);

export default function RadioButtons({
    data = [],
    ...props
}) {
    const [value, setValue] = React.useState(null);

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <FormControl component="fieldset">
            <RadioGroup value={value} onChange={handleChange}>
                {
                    data.map((item, index) => {
                        return (
                            <Grid>

                                {
                                    item.value == 0 ?
                                        <FormControlLabel value={item.value.toString()} control={<Radio />}
                                            label={
                                                <Typography style={{ fontSize: 16, color: "#707070" }}>
                                                    <TextField disabled={value != 0} placeholder="อื่นๆ โปรดระบุ" />
                                                </Typography>} />

                                        :
                                        <FormControlLabel value={item.value.toString()} control={<Radio />} label={<Typography style={{ fontSize: 16, color: "#707070" }}>{item.title}</Typography>} />
                                }
                            </Grid>

                        )
                    })

                }

            </RadioGroup>
        </FormControl>
    );
}
