import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button as ButtonMaterial, Grid } from '@material-ui/core';


export default function Button({ text, ...props }) {
    const classes = useStyles();

    return (
        <Grid className={classes.root}>
            <ButtonMaterial style={{ backgroundColor: "#F38181", color: "#FFFFFF", width: "100%" }} {...props} variant="contained">{text}</ButtonMaterial>
        </Grid>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
        width: "100%"
    },
}));