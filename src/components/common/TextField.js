import React from 'react';
import { createMuiTheme, makeStyles, TextField as TextInput, ThemeProvider } from '@material-ui/core'


export default function TextField({ ...props }) {

    const theme = createMuiTheme({
        palette: {
            primary: { 500: "#F38181" },
        },
    });

    const classes = useStyles();
    return (
        <ThemeProvider theme={theme}>
            <TextInput
                id="TextField"
                variant="outlined"
                // InputLabelProps={{
                //     shrink: true,
                // }}
                {...props}
                className={classes.inputText}
            />
        </ThemeProvider>
    )
}


const useStyles = makeStyles((theme) => ({
    inputText: {
        width: "100%",
        textAlign: "start"
    }
}));

