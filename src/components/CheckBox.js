import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import { Checkbox as CheckBoxUI, Grid } from '@material-ui/core';
import TextField from './common/TextField';



export default function CheckBox({
    data = [],
    ...props
}) {
    const classes = useStyles();
    const [state, setState] = React.useState(data);
    const handleChange = (event) => {
        const dataUpdate = [...state];
        const index = event.target.value;
        dataUpdate[index] = {
            ...dataUpdate[index], value: event.target.checked
        }
        setState([...dataUpdate])
    };


    return (
        <div className={classes.root}>
            <FormControl component="fieldset" className={classes.formControl}>
                <FormGroup>
                    {
                        state.map((item, index) => {
                            return (
                                <Grid>
                                    {
                                        item.title == "อื่นๆ โปรดระบุ" ?

                                            <FormControlLabel
                                                control={<CheckBoxUI checked={item.value == true} onChange={handleChange} value={index} />}
                                                label={
                                                    <TextField disabled={state.filter((item) => item.title == "อื่นๆ โปรดระบุ")[0].value != true} placeholder="อื่นๆ โปรดระบุ" />
                                                }
                                            />
                                            :
                                            <FormControlLabel
                                                control={<CheckBoxUI checked={item.value == true} onChange={handleChange} value={index} />}
                                                label={item.title}
                                            />
                                    }
                                </Grid>

                            )
                        })
                    }

                </FormGroup>
            </FormControl>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
}));