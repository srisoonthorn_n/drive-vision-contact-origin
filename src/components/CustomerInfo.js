import React from 'react'
import { Card, Grid, makeStyles, Typography } from '@material-ui/core'
import TextField from './common/TextField';

export default function CustomerInfo() {
    const classes = useStyles();
    return (
        <Grid container item xs={11} md={8} lg={6} direction="column" style={{ marginBottom: 33 }}>

            <Grid container item xs={11} style={{ marginBottom: 20 }}>
                <Typography style={{ fontSize: 28, fontWeight: "bold", color: "#707070" }}>ข้อมูลลูกค้า</Typography>
            </Grid>

            <Card style={{ padding: 40 }}>
                <Grid container direction="column" item xs={12} style={{ marginBottom: 8 }}>

                    <Grid style={{ marginBottom: 16 }}>
                        <Grid>
                            <TextField label="ชื่อ-นามสกุล" placeholder="ชื่อ - นามสกุล" />
                        </Grid>
                    </Grid>

                    <Grid style={{ marginBottom: 16 }}>

                        <Grid>
                            <TextField
                                label="Email" placeholder="test@test.com" />
                        </Grid>
                    </Grid>

                    <Grid container direction="row" spacing={2} style={{ marginBottom: 16 }}>

                        <Grid container item xs={12} lg={6} >
                            <TextField
                                label={
                                    <Grid container direction="row">
                                        <Typography style={{ fontSize: 16 }}>เบอร์โทรติดต่อ</Typography>
                                        <Typography style={{ color: "#F38181", marginLeft: 8 }}>*</Typography>
                                    </Grid>
                                }
                                placeholder="08x-xxx-xxxx" />
                        </Grid>

                        <Grid container item xs={12} lg={6} >
                            <TextField
                                label="Line ID"
                                placeholder="test1234" />
                        </Grid>

                    </Grid>


                    <Grid style={{ marginBottom: 16 }}>
                        <Grid>
                            <TextField multiline rows={6} label="ที่อยู่" placeholder="ที่อยู่" />
                        </Grid>
                    </Grid>

                </Grid>
            </Card>

        </Grid>
    )

}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: 24
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    listDrawer: {
        width: 250,
    },
    fullListDrawer: {
        width: 'auto',
    },
}));
