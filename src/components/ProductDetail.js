import React from 'react';
import { Grid, Hidden, makeStyles, Typography } from '@material-ui/core';
import ImagesPhone from "../assets/images/mockup_ipad-iphone-destop.png"

export default function ProductDetail() {
    const classes = useStyles();
    return (
        <Grid container justify="center" alignItems="center" item xs={12}
            style={{ backgroundColor: "#FFC5C5", paddingTop: 24, paddingBottom: 24, marginBottom: 24 }}
        >
            <Grid item xs={"auto"} sm={"auto"} md={"auto"} lg={"auto"} style={{ paddingBottom: 8, paddingTop: 8, marginRight: 16, marginLeft: 16 }}>
                {/* <Grid container alignItems="flex-end" style={{ backgroundColor: "#FFFFFF", width: 126, height: 126, borderRadius: 12 }} /> */}
                <img src={ImagesPhone} style={{ width: "100%", maxWidth: 384, maxheight: 256 }} />
            </Grid>

            <Hidden xsDown>
                <Grid item xs={11} sm={8} md={6} lg={5} style={{ paddingLeft: 8 }}>
                    <Grid style={{ marginBottom: 20 }}>
                        <Typography style={{ fontSize: 28, fontWeight: "bold", color: "#FFFFFF" }}>ข้อมูลสินค้า</Typography>
                    </Grid>
                    <Grid style={{ backgroundColor: "#FFFFFF", paddingInline: 40, paddingBlock: 25, borderRadius: 16 }}>
                        <Typography style={{ fontSize: 16, color: "#707070" }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea rud exercitation ullamco laboris nisi ut aliquip ex ea
                    </Typography>
                    </Grid>
                </Grid>
            </Hidden>

            <Hidden smUp>
                <Grid item xs={11} sm={8} md={6} lg={5} style={{ paddingLeft: 8 }}>
                    <Grid style={{ marginBottom: 20 }}>
                        <Typography style={{ fontSize: 28, fontWeight: "bold", color: "#FFFFFF", textAlign: "center" }}>ข้อมูลสินค้า</Typography>
                    </Grid>
                    <Grid style={{ backgroundColor: "#FFFFFF", paddingInline: 40, paddingBlock: 25, borderRadius: 16 }}>
                        <Typography style={{ fontSize: 16, color: "#707070" }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea rud exercitation ullamco laboris nisi ut aliquip ex ea
                    </Typography>
                    </Grid>
                </Grid>
            </Hidden>



        </Grid>
    )

}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: 24
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    listDrawer: {
        width: 250,
    },
    fullListDrawer: {
        width: 'auto',
    },
}));
