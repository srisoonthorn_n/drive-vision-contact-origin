import React from 'react'
import { Card, Grid, makeStyles, Typography } from '@material-ui/core'
import TextField from './common/TextField';

export default function CompanyInfo() {
    const classes = useStyles();
    return (
        <Grid container item xs={11} md={8} lg={6} direction="column" style={{ marginBottom: 33 }}>
            <Grid container item xs={11} lg={11} style={{ marginBottom: 20 }}>
                <Typography style={{ fontSize: 28, fontWeight: "bold", color: "#707070" }}>ข้อมูลบริษัท</Typography>
            </Grid>

            <Card style={{ padding: 40 }}>
                <Grid container direction="column" item xs={12} style={{ marginBottom: 8 }}>

                    <Grid style={{ marginBottom: 16 }}>
                        <TextField label="ชื่อบริษัท" placeholder="test company" />
                    </Grid>

                    <Grid style={{ marginBottom: 16 }}>
                        <TextField label="ที่อยู่" placeholder="ที่อยู่" multiline rows={6} />
                    </Grid>

                </Grid>
            </Card>

        </Grid>
    )

}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: 24
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    listDrawer: {
        width: 250,
    },
    fullListDrawer: {
        width: 'auto',
    },
}));
