import React from 'react'
import { Card, Grid, makeStyles, Typography } from '@material-ui/core'
import TextField from './common/TextField';
import RadioButtons from './common/RadioButtons';
import CheckBox from './CheckBox';

export default function BusinessInfo() {
    const classes = useStyles();
    const BusinessMockInfo = {
        Business: [
            { ID: 1, title: "โรงน้ำแข็ง", value: 1 },
            { ID: 2, title: "ร้านน้ำ", value: 2 },
            { ID: 3, title: "ร้านค้าขายส่ง", value: 3 },
            { ID: 4, title: "อื่นๆ โปรดระบุ", value: 0 },
        ],
        CustomerNeed: [
            { ID: 1, title: "มี QR เช็คยอดเบิกก่อนทำส่งสินค้า", value: false },
            { ID: 2, title: "คำนวณและแจ้งเตือนสินเชื่อและค้างจ่าย", value: false },
            { ID: 3, title: "ระบบนำทางคนขับ", value: false },
            { ID: 4, title: "คำนวนบัญชีอัตโนมัติ", value: false },
            { ID: 5, title: "ติดตามยอดการซื้อ-ขาย แบบ Realtime", value: false },
            { ID: 6, title: "อื่นๆ โปรดระบุ", value: false },
        ],
        Budget: [
            { ID: 1, title: "0 - 300฿", value: 1 },
            { ID: 2, title: "301 - 500฿", value: 2 },
            { ID: 3, title: "501 - 1000฿", value: 3 },
            { ID: 4, title: "1000฿ หรือ มากกว่า", value: 4 },
        ]
    }


    return (
        <Grid container item xs={11} md={8} lg={6} direction="column" style={{ marginBottom: 33 }}>
            <Grid container item xs={11} style={{ marginBottom: 20 }}>
                <Typography style={{ fontSize: 28, fontWeight: "bold", color: "#707070" }}>ข้อมูลธุรกิจ</Typography>
            </Grid>

            <Card style={{ padding: 40 }}>
                <Grid container direction="column" style={{ marginBottom: 8 }}>
                    <Grid style={{ marginBottom: 32 }}>
                        <Grid container direction="row" style={{ marginBottom: 28 }}>
                            <Grid>
                                <Typography style={{ fontSize: 20, color: "#707070" }}>ประเภทของธุรกิจ</Typography>
                            </Grid>
                            <Grid>
                                <Typography style={{ marginLeft: 8, fontSize: 20, color: "#F38181" }}>*</Typography>
                            </Grid>

                        </Grid>

                        <Grid>
                            <RadioButtons data={BusinessMockInfo.Business} />
                        </Grid>
                    </Grid>

                    <Grid style={{ marginBottom: 32 }}>
                        <Grid container direction="row" style={{ marginBottom: 28 }}>
                            <Typography style={{ fontSize: 20, color: "#707070" }}>ความต้องการในการใช้งาน</Typography>
                        </Grid>

                        <Grid>
                            <CheckBox data={BusinessMockInfo.CustomerNeed} />
                        </Grid>
                    </Grid>

                    <Grid style={{ marginBottom: 32 }}>
                        <Grid container direction="row" style={{ marginBottom: 28 }}>
                            <Grid>
                                <Typography style={{ fontSize: 20, color: "#707070" }}>งบประมาณในการจัดซื้อซอฟแวร์</Typography>
                            </Grid>
                            <Grid>
                                <Typography style={{ marginLeft: 8, fontSize: 20, color: "#F38181" }}>*</Typography>
                            </Grid>

                        </Grid>

                        <Grid>
                            <RadioButtons data={BusinessMockInfo.Budget} />
                        </Grid>
                    </Grid>

                </Grid>
            </Card>

        </Grid>
    )

}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: 24
    },
}));
