import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { Grid } from '@material-ui/core';

import LogoIcon from '../assets/images/delivery.png'

export default function AppBarComponent() {
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };



    const list = () => (
        <div
            className={clsx(classes.listDrawer)}
            role="presentation"
            onClick={handleDrawerOpen}
            onKeyDown={handleDrawerOpen}
        >
            <List>
                {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            <List>
                {['All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div >
    );

    return (
        <div className={classes.root}>
            <Drawer anchor="left" open={open} onClose={handleDrawerClose}>
                {list()}
            </Drawer>

            <AppBar position="static">
                <Toolbar className={classes.appBarStyle}>
                    <IconButton
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    <Grid container direction="row" alignItems="center" >

                        <Grid style={{ marginRight: 8 }}>
                            <img
                                src={LogoIcon}
                                className={classes.iconAppBar}
                            />
                        </Grid>

                        <Grid>
                            <Typography variant="h6" className={classes.title}>
                                DriveVision
                            </Typography>
                        </Grid>

                    </Grid>

                </Toolbar>
            </AppBar>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        // marginBottom: 24
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    listDrawer: {
        width: 250,
    },
    fullListDrawer: {
        width: 'auto',
    },
    appBarStyle: {
        backgroundColor: "#F38181"
    },
    iconAppBar: {
        width: 28,
        height: 28,
        paddingTop: 8
    },


}));
