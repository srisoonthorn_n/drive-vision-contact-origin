import './App.css';
import { ThemeProvider } from '@material-ui/core';
import ContactScreen from './screens/ContactScreen';
import AppBarComponent from './components/AppBarComponent';
import Footer from './components/Footer';

function App() {
  return (
    <ThemeProvider>
      <AppBarComponent />
      <ContactScreen />
      <Footer width={'100%'}/>
    </ThemeProvider>
  );
}

export default App;
